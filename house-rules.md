% House rules

Turn taking
-----------

In all but the last round:

If everyone is waiting for someone else to decide whether to draw (or
place a deferred chip), the person with the emptiest pot chooses (and
if necessary draws) first.  Ties are broken by the Fortune Teller card
turn order.

[Rationale: the official rule suffers from a perverse incentive.
This rule approximates natural player behaviour, formalising the edge
case.]


Greens etc.
-----------

When a chip (typically green, but also eg some blacks) talks about "the
last or second-last chip in your pot", apply the rule for each of the
last *three* chips, not just the last *two*.

[Rationale: Greens seem not to come up.  In my experience, even with
this change they are still quite weak.]


Overflow
--------

We have overflow tracks, not overflow bowls.  They are simply an
extension of the pot's track.  The space "34 14" on the overflow track
follows the space "33 14 ruby" in the main pot.  Disregard (cover up)
the "35 15" on the provided board.


### Buying three chips (etc.)

In the "money" phase (and any other time you are invited to buy
chips), you may buy *one* additional chip, but to do so you must pay 5
*extra* money (in addition to the price of the additional chip).

(You may combine this with the "Sale" Fortune Teller card to get the
another of cheapest chip, for a total of 4 chips.)


Alchemists ("diseases")
-----------------------

Players do not choose their own disease.  Instead, choose one disease
randomly, for all the players.

If your essence value is more than 10, you get the bonus for 10,
*plus* the bonus for the remainder (max.10).  (Notate this by placing
two counters on the essence track).  With **Vampirism**, add the
buying power together and buy one chip.  With essences that are used
up during play by reducing your essence, you may make up a reduction
by adding two reductions of your two essence tokens.

### Ear Worm

In the last round, perform the essence bonus, instead of getting VPs
for your essence.

### Nervousness

In the final round, when drawing is supposed to be secret and
simultaneous:

Each player with Nervousness chips puts some large draughts stones in
their bag.  When drawing a chip, they may (by feel) choose to draw a
draughts stone instead.  When the stone is revealed, it means they are
placing one of their Nervousness chips.

A player who drew a stone selects *which* of their Nervousness chips
they are playing before anyone makes other decisions about how to play
their chips.  If more than one player has more than one Nervousness
chip to choose from, the applicable players each choose a Nervousness
chip in fortune teller card turn order.


Blacks book V
-------------

If you have *any* black in the last three, you get rubies for *all*
your own blacks.

[Rationale: this adds an element of diminishing returns for the player
*buying* the black, which was otherwise missing.  The official rules
text is ambiguous.]


Blacks book VI
--------------

You are not eligible for the ruby if you got the droplet.

[Rationale: these are somewhat be overpowered, especially if you have
many.  We want to debuff them.]

No ruby awarded if more than one person got the droplet.

[Rationale: As above, but also this is a usual 1st/2nd prize rule.]


Locoweed book VI "last coloured chip"
-------------------------------------

This chip costs 9, not 10.

[Rationale: This chip is hardly ever a good buy at 10.]


Fortune teller "Neighbour in Need"
----------------------------------

We do not play with this card.  Draw another one instead.

(We think it means that you get to choose between (a) the player on
your left may remove a garlic and (b) they may take a chip of their
choice.)

Fortune teller Visit from a Witch
---------------------------------

We play with this card even if we are not playing with the Herb
Witches.


CLARIFICATIONS
==============


General
-------

Any bonus chips you gain after the start of a round are put aside and
go into your bag in the next round.  This is equivalent to the
official wording but more generalisable.  In particular, it applies to
the bronze witch "exchange your ... chips".  (Chips from **Witch's Hump**
go into your bag, as per the official rules.)

You may always use a chip's special power as if it were a chip of a
lower value of the same colour.  (That is, for a 4-chip you can choose
to use the corresponding 2- or 1-chip power; for a 2-chip, the 1-chip
power.)

But you may not forgo a chip's special power unless it says so (eg, by
saying "may").

A locoweed is not a "1-chip".  But it has a "value" (the number of
spaces to move), unless otherwise specified.

By no means may you acquire a purple or yellow before they are
available (from the start of rounds 2 and 3).

For the avoidance of doubt: when you return a chip to your bag, any
special effects that have already occurred does not need to be undone.
But any ongoing or end-of-round effects (or side-effects) no longer
happen because the returned chip is no longer in your pot: henceforth
it is henceforth as if that chip was never placed.

(As a general principle, rules interactions do not involve any hidden
state that doesn't exist for the rules taken separately.)

Books
-----

### Yellow III (increased garlic threshold)

If you remove a yellow from your pot (due to some rule or other), and
this reduces your garlic threshold below your current amount of
garlic, your pot *does* explode.

Witches
-------

### Silver "Receive the full bonus if exploded"

Using this witch does not deprive anyone else of the die roll.


Alchemists ("diseases")
-----------------------

### Chicken eyes

When upgrading chips, the new ones go into your pot, in the same
space as the old chips.

When upgrading a 1-chip to a 4-chip, you may upgrade a 2-chip instead.

### Ear worm

Draw all the Ear Worm chips at once; you may choose the placement
order.

### Nervousness

With **Book V Yellow**, the chip for the yellow must come from your bag,
not from Nervousness chips.

### Witch's Hump

You may take the bonus for to a lower-valued chip, instead.

Giant pumpkin (on a ruby space) counts as 4-chip or locoweed.

Fortune teller cards
--------------------

(Clarifications of interactions with other rules are here, rather than
filed under the other rule or game feature.)

Any VPs you gain as a result of a fortune teller card don't affect the
number of rat tails you get this turn; likewise, if your droplet
moves, move your rat stone too.

### From Good to Better

With **Ear Worm**, do Ear Worm first.

### Good neighbourhood

You may do this as many times as you like.

### Less is More

Locoweed count for 1 each.

### Pot is Full

*Every* die roll is to be rolled twice, not just the one for winning
the round.

### Pumpkin Patch Party

With Alchemists' **Carrot Nose**, Carrot Nose happens first.

### Rat Infestation

Doubles all rats, no matter where they come from, not just VP track
rats.

### Sale

This applies only to the normal "money" phase, not any other rules
which might involve buying chips.

If buying two chips of the same value, you can choose which to receive
another of for free.

### Second Chance

If you are unlucky enough to draw within the first 5 chips, a garlic
that would make you explode, you may put everything back in your bag
and restart immediately -- before the explosion happens.  (Assuming
you haven't restarted already, obviously.)

Other side effects that occurred in your first 5 chips are not undone.

**Nervousness** (alchemists): if you choose to start again, you put all
your Nervousness chips back into your bag, and choose a fresh set for
the restart.

### Small Donation

Applies to this round only (despite the wording which suggests
otherwise).

### Strong Ingredient

With **Book II Red**: draw 5 for Strong Ingredient before placing any
reds.  Then place Strong Ingredient chips, and any reds you wish to
place, in any order.

### White Blessing

With **Book II Red**, place any reds into your pot first, before resolving
the White Blessing.

* * *

| Books of Pumpkins, etc.  Extensions to Quacks of Quedlinburg.
| This file: Copyright 2020-2021 Ian Jackson.
| `SPDX-License-Identifier: GPL-3.0-or-later OR CC-BY-SA-4.0`.
