
Test tubes vs pot droplet vs rats
---------------------------------

You may not advance your test tube droplet unless your pot+rat is at
least level with everyone else's pot+rat.  Otherwise, you must advance
your pot droplet.

Subtle points: (i) For these purposes, your rat is placed in your pot
*before* drawing the Fortune Teller card; but its position is adjusted
again as necessary *after* (or as a result of) the Fortune Teller
card, as usual.  (ii) If you advance your pot droplet and have your
rat in the pot, advance your rat too.

[Rationale: test tube droplets seem overpowered.]


* * *

| Books of Pumpkins, etc.  Extensions to Quacks of Quedlinburg.
| This file: Copyright 2020 Ian Jackson.
| `SPDX-License-Identifier: GPL-3.0-or-later OR CC-BY-SA-4.0`.
