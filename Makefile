# Books of Pumpkins, etc.  Extensions to Quacks of Quedlinburg
#  SPDX-License-Identifier: GPL-3.0-or-later OR CC-BY-SA-4.0
# Copyright 2020-2021 Ian Jackson

PUBLISH_MD += pumpkin-books house-rules
PUBLISH += pumpkin-books.pdf overflow-tube.pdf index.html
PUBLISH += README.html LICENCE.txt GPL-3.txt CC-BY-SA-4.0.txt
PUBLISH += $(addsuffix .md.txt,  $(PUBLISH_MD))
PUBLISH += $(addsuffix .md.html, $(PUBLISH_MD))
PUBLISH += $(addsuffix .md.pdf,  $(PUBLISH_MD))

SHELL=bash

BOOKS+=		red
BOOKS+=		green
BOOKS+=		purple
BOOKS+=		blue
BOOKS+=		lotus
BOOKS+=		yellow
BOOKS+=		black-pair/black-more

BOOK_STEMS := $(addprefix book-,$(subst /, ,$(BOOKS)))
PUBLISH += $(addsuffix .pdf, $(BOOK_STEMS))
PUBLISH += $(addsuffix .png, $(BOOK_STEMS))

all:	$(PUBLISH)

# Don't print the ps, printers (eg ours!) don't always have the fonts

i=mv -f $@.new $@
o=>$@.new && $i
use_rsvg=rsvg-convert -f png -o $@.tmp.png $<
shell_pipe=set -o pipefail; set -e;
make_pnm = pnmdepth 255 | pnmnoraw $o

%.ps: %.ps.pl bag.pgm pot.pgm droplet.ppm
	./$< $(BOOKS) $o

%.pdf: %.ps
	gs -q -dNOPAUSE -dBATCH -dPDFSETTINGS=/prepress \
		-sDEVICE=pdfwrite -sOutputFile=$@ $<

bag.pgm: Sack_-_Sac_-_Zak_brown.svg Makefile
	$(use_rsvg)
	$(shell_pipe)					\
	pngtopnm <$@.tmp.png -mix -background 'white'	\
	| ppmtopgm 					\
	| $(make_pnm)

pot.pgm: 146_Kaldrono.svg Makefile
	$(use_rsvg)
	$(shell_pipe)					\
	pngtopnm <$@.tmp.png 				\
	| ppmtopgm 					\
	| pnminvert 					\
	| $(make_pnm)

droplet.ppm: droplet.svg Makefile
	$(use_rsvg)
	$(shell_pipe)					\
	pngtopnm <$@.tmp.png -mix -background 'white'	\
	| pnmdepth 255					\
	| $(make_pnm)

$(addsuffix .ps, $(BOOK_STEMS)): pumpkin-books.ps
	@:

book-%.pdf: book-%.eps
	rm -f $@.new; ps2pdf $< $@.new && $i

book-%.eps: book-%.ps
	ps2eps <$< $o

book-%.png: book-%.eps Makefile
	gm convert -density 100x100 $< $@

.PRECIOUS: book-%.eps

PUBLISH_FILES= \
	$(patsubst %.md,%.md.txt, $(PUBLISH)) \
	$(patsubst %.md,%.md.html, $(filter $(PUBLISH)) \

index.html: gen-index README.html
	./$^ $(BOOK_STEMS) $o

%.md.txt: %.md
	cat $< $o

%.md.html: %.md Makefile massage-html
	pandoc -s -Mtitle='Fm4YNbKyBNiS7TNu' $< >$@.tmp
	./massage-html $< $@.tmp $o

%.md.pdf: %.md Makefile
	perl -pe '$$_ = uc if $$.==1' <$< >$@.tmp
	pandoc --pdf-engine=wkhtmltopdf -o $@ $@.tmp

pubssh=c
pubpath=games-rules/quacks
puburl=https://www.chiark.greenend.org.uk/~ijackson/

publish: $(PUBLISH)
	git push origin
	rsync $^ $(pubssh):public-html/$(pubpath)/.
	@echo $(puburl)$(pubpath)/README.html
