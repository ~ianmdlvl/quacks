% Books involving pumpkins


Choose books randomly, and include the books below as options.

[Rationale: a friend was disappointed after playing Book 1 to discover
that none of the other books in the base game do anything with
pumpkins.]


GREEN
-----
5/9/15

For each pumpkin in the last 3 chips (but not more than the number of
green chips in your pot), receive one ruby.

For example, if you have 2 pumpkins in the last three chips placed,
and one green in the pot, you get one ruby.


YELLOW
------
5/11/19

Find up to 1/2/4 pumpkins already placed in your pot.  Move them to
right after the yellow.  You may place the moved pumpkins in any
order.

Each moved pumpkin is placed in the next available space, regardless
of its value or of other rules, and do not perform any chip or board
actions.

Any other effects of placing these chips there, do happen.  If the
"pumpkin" was actually a "last coloured chip" Lotus, it can mutate, so
that when it is being re-placed it might be a yellow.


RED
---
4/9/16

The next 1/2/4 pumpkins you place your pot are each moved one extra
space.  (After applying any other special effects; only one extra
space no matter how many reds.)


BLUE
----
4/9/17

If the previous chip played was a pumpkin, you may exchange it with
the supply as follows:

 Blue 1              =>  Green 1 / Red 1 / Yellow 1 / Blue 1
 Blue 2              =>  Any 2 chip / Purple / Black / Lotus
 Blue 4              =>  Any 4 chip

(The new chip should be put aside, to go into your bag in the next
round.)


PURPLE
------
10

For each pumpkin in the pot (but not more than the number of purple
chips), add up the VPs of the covered spaces.  Buy 1/2 chips of up to
that value.

(NB, buy according to the VP values of the covered spaces, not the
money values - like the purple in Book IV.)


BLACK
-----
10

Find the shortest distance (spaces along the pot track) between a
pumpkin and a black.  (The black may be before or after the pumpkin.)

Two players: shortest distance gets to move their droplet one space.
Equal distance gets a ruby each.

More players: 1st prize = droplet; 2nd prize = ruby.
(Winner of the 1st prize is not eligible for the 2nd prize.
No 2nd prize awarded if joint winners of 1st prize.)


LOCOWEED
--------
8

The value of this chip is 1 higher than the number of pumpkins in the
pot (but the value is capped at 4).



* * *

| Books of Pumpkins, etc.  Extensions to Quacks of Quedlinburg.
| This file: Copyright 2020-2021 Ian Jackson.
| `SPDX-License-Identifier: GPL-3.0-or-later OR CC-BY-SA-4.0`.
