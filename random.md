RANDOM INITIAL BAG NOTES
========================

We don't understand the balance implications, which are likely to be
nontrivial, so both players should get the same starting bag.


I. Garlic
---------

# Assumption 1

(Pretty certain.)  The total amount of garlic is important.  It must
be 11 (4x1 + 2x2 + 3) until the 1 extra gets added.

# Assumption 2

(Doubtful, may needs testing.)

The amount of "big" garlic vs "small" garlic chips is important.
Reify this as: the total number of garlic chips must remain (roughly)
the same.

# Options

With assumptions 2, there are only the following garlic compositions
possible:

X.  2x 3-garlic [4]                     5x 1-garlic
Y.  1x 3-garlic [2]   2x 2-garlic [2]   4x 1-garlic   <- standard
Z.  0x 3-garlic       4x 2-garlic [4]   3x 1-garlic

(Proof: there are 7 garlic chips, each of at least 1 garlic.  That
leaves 4 "surplus" garlic ("[]") to distribute amongst
otherwise-identical chips.)

With assumption 2 weakened to "roughly" (ie, +1/-1) there are the
following additional possibilities:

1 fewer garlicky chips (6, more lumpy garlic, 5 "surplus"):

P.  2x 3-garlic [4]   1x 2-garlic [1]   3x 1-garlic
Q.  1x 3-garlic [2]   3x 2-garlic [3]   2x 1-garlic
R.  0x 3-garlic       5x 2-garlic [5]   1x 1-garlic

1 more garlicky chips (8, less lumpy garlic, 3 "surplus"):

S.  1x 3-garlic [2]   1x 2-garlic [1]   6x 1-garlic
T.  0x 3-garlic       3x 2-garlic [3]   5x 1-garlic

With assumption 2 removed entirely, there are many other possiblities
but they seem very strange.


# Suggested garlic variations

 A. Prepare, for each player, one each of X Y Z.  Choose secretly and
    randomly.

 B. Same, but include P..T.  Players are entitled to count their
    chips so they will know the difference between XYZ/PQR/ST.


II. Non-garlic
--------------

# Discussion

Standard load is 1 pumpkin (¤3) plus 1 green (about ¤5).

Options would seem to include:

 * Replace the green with a red, blue or locoweed.  These are
   available at this point and relativity uncontroversial (although
   probably of higher value, albeit that the locoweed is not very
   interesting).

 * Replace the green with a black.  This is interesting because blacks
   are often very important but become less so with increased
   availability.

 * Replace the green with a yellow or purple.  These are not available
   for purchase at this time.  This seems anomalous.

 * Do something about the pumpkin.  Replacing it with a proper chip
   seems very bold.  But perhaps there could be several.

Increasing the starting bag value too much seems a bad idea because we
sometimes run off the end.


# Suggested non-garlic variations:

 K. Initial bag with 1 pumpkin, plus one random colour chosen from
    green/red/blue/locoweed/black.

 L. Initial bag with 3 pumpkins.

 M. Initial bag with jsut garlic; each player may buy, at the start,
    in turn order for notional turn 0, chips to the value of 8.


III. Mechanics
--------------

# Random choice

To choose secretly and randomly between identical-looking sets of
containers (let's call them bags, supposing they're XYZ from "Garlic",
above).

All players collaborate to parepare bags, one for each player,
according to each of XYZ.  So now (for eg two players) we have bags:
  X0 Y0 Z0
  X1 Y1 Z1
Place these in pairs in locations labelled X, Y, Z.  So we ahve:
  X: bag, bag, ...
  Y: bag, bag, ...
  Z: bag, bag, ...

Everyone but player 1 leaves the room.  Player 1 rolls dice
to randomly move move bag-pairs X Y Z to new locations alpha, beta,
gamma, according to a random permutation.  Now we have:
  alpha: bag, bag, ...
  beta:  bag, bag, ...
  gamma: bag, bag, ...

Everyone but player 1 leaves the room.  Player 2's rolls a die to
select one of alpha, beta, or gamma.  The others are put aside:
  selected: bag, bag, ...
  set aside (secret): many bags

At the end of the game, everyone must reveal their bags.  Perhaps we
declare that at the start of round 5 (at the extra garlic) everyone
must do so, and after that people may look in their bag.


# Combination

If multiple choices must be made, containers other than bags can be
used.  Eg 35mm film canisters.  They must be unloaded into the bags by
the players, without looking at them.
